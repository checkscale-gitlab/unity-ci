﻿using NUnit.Framework;
using System.Collections;
using UnityEngine.TestTools;

// Tests are divided in 3 parts
// 1 - ARRANGE all preconditions and inputs
// 2 - ACT on the method under test
// 3 - ASSERT occurrence of expected results
public class PointsCalculatorTests
{
    [Test]
    public void CalculateTotalPoints_ExampleTest()
    {
        // Arrange
        PointsCalculator pointsCalculator = new PointsCalculator();
        int killedEnemies = 24;
        int killedHostages = 3;
        int expectedPoints = (24 * 100 - 3 * 500); // Adding +1 will fail the test
        
        // Act
        int points = pointsCalculator.CalculateTotalPoints(killedEnemies, killedHostages);

        // Asset
        Assert.That(points, Is.EqualTo(expectedPoints));
    }

    [UnityTest]
    public IEnumerator UnityTest_ExampleTest()
    {
        Assert.IsTrue(true);
        yield return null;
    }
}